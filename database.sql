Use `container-db`;

DROP TABLE IF EXISTS Content;
DROP TABLE IF EXISTS Contact_Form;
DROP TABLE IF EXISTS Appointment_request;
--CREATE TABLE
-- CREATE TABLE Content (
--   ContentID INT(11)  NOT NULL auto_increment,
--   PageIdentifier varchar(255) NOT NULL,
--   Content TEXT NOT NULL,
--   PRIMARY KEY (CONTENTID)
-- ) AUTO_INCREMENT=1;

CREATE TABLE Contact_Form(
formID int(11) NOT NULL auto_increment,
Fname varchar(255) NOT NULL,
LNAME varchar(255) NOT NULL,
Email varchar(255) NOT NULL,
Phone int (11) NOT NULL,
Question VARCHAR (255) NOT NULL,
PRIMARY KEY (formID)
)AUTO_INCREMENT=1;

CREATE TABLE Appointment_request(
RequestID int(11) NOT NULL auto_increment,
Fname varchar(255) NOT NULL,
LNAME varchar(255) NOT NULL,
Email varchar(255) NOT NULL,
Phone int (11) NOT NULL,
_Subject VARCHAR (255) NOT NULL,
TD VARCHAR (255) NOT NULL,
PRIMARY KEY (RequestID)
)AUTO_INCREMENT=1;

-- --CREATE DATA
-- INSERT INTO Content (PageIdentifier,Content) VALUES ('Home','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing');
-- INSERT INTO Content (PageIdentifier,Content) VALUES ('Contact page','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur');
-- INSERT INTO Content (PageIdentifier,Content) VALUES ('About us','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus.');
-- INSERT INTO Content (PageIdentifier,Content) VALUES ('Advice','Lorem ipsum dolor sit');
-- INSERT INTO Content (PageIdentifier,Content) VALUES ('Useful links','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur');
-- INSERT INTO Content (PageIdentifier,Content) VALUES ('Request page','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer');

-- INSERT INTO Contact_Form (FNAME,LNAME,Email,phone,Question) VALUES ('Reed','Castaneda','eleifend.nunc.risus@liberonecligula.co.uk','(03) 3390 5067','fringilla ornare placerat, orci lacus vestibulum');
-- INSERT INTO Contact_Form (FNAME,LNAME,Email,phone,Question) VALUES ('Caldwell','Page','nec@Morbisit.com','(02) 7792 0142','sem egestas blandit. Nam nulla');
-- INSERT INTO Contact_Form (FNAME,LNAME,Email,phone,Question) VALUES ('Solomon','Sanford','lorem.vitae.odio@tellusNunc.edu','(04) 2044 0897','rhoncus.');
-- INSERT INTO Contact_Form (FNAME,LNAME,Email,phone,Question) VALUES ('Octavius','Blake','Curabitur.vel.lectus@elementumat.ca','(09) 9647 3388','pharetra. Nam ac');
-- INSERT INTO Contact_Form (FNAME,LNAME,Email,phone,Question) VALUES ('Chaney','Boone','sit.amet.lorem@inconsequatenim.org','(03) 6209 7642','ante');
-- INSERT INTO Contact_Form (FNAME,LNAME,Email,phone,Question) VALUES ('Herrod','Beasley','sodales.purus.in@acfeugiatnon.ca','(08) 0848 4057','Nunc mauris. Morbi');

-- INSERT INTO Appointment_request (FNAME,LNAME,Email,phone,_Subject,TD) VALUES ('Reed','Castaneda','eleifend.nunc.risus@liberonecligula.co.uk','(03) 3390 5067','fringilla ornare placerat, orci lacus vestibulum','May 23rd, 2018');
-- INSERT INTO Appointment_request (FNAME,LNAME,Email,phone,_Subject,TD) VALUES ('Caldwell','Page','nec@Morbisit.com','(02) 7792 0142','sem egestas blandit. Nam nulla','October 5th, 2017');
-- INSERT INTO Appointment_request (FNAME,LNAME,Email,phone,_Subject,TD) VALUES ('Solomon','Sanford','lorem.vitae.odio@tellusNunc.edu','(04) 2044 0897','rhoncus.','December 17th, 2018');
-- INSERT INTO Appointment_request (FNAME,LNAME,Email,phone,_Subject,TD) VALUES ('Octavius','Blake','Curabitur.vel.lectus@elementumat.ca','(09) 9647 3388','pharetra. Nam ac','September 25th, 2018');
-- INSERT INTO Appointment_request (FNAME,LNAME,Email,phone,_Subject,TD) VALUES ('Chaney','Boone','sit.amet.lorem@inconsequatenim.org','(03) 6209 7642','ante','August 1st, 2018');
-- INSERT INTO Appointment_request (FNAME,LNAME,Email,phone,_Subject,TD) VALUES ('Herrod','Beasley','sodales.purus.in@acfeugiatnon.ca','(08) 0848 4057','Nunc mauris. Morbi','March 28th, 2018'));